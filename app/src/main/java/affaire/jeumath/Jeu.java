package affaire.jeumath;

import java.util.Observable;

public class Jeu extends Observable {

    int min, max;
    Question questionCourante;

    public Jeu(){
        recupererMinMax();
    }

    /**
     * À faire: Aller récupérer le min et le max dans les préférences
     */
    private void recupererMinMax() {
        //TODO Récupérer les valeurs depuis les préférences
        min = -10;
        max = 10;
    }

    public Question prochaineQuestion(){
        questionCourante = new Question(min, max);
        return  questionCourante;
    }

    public void valider(int reponse){
        if(reponse == questionCourante.getReponse()){
            notifier("Bon");
        }
        else{
            notifier("Mauvais");
        }

        sauvegarder();
    }

    /**
     * À faire: Sauvegarder la tentative dans une base de données locale
     */
    private void sauvegarder() {
        //TODO créer une base de données et enregistrer
        // 1- La question
        // 2- Les choix des réponses
        // 3- La bonne réponse
        // 4- La réponse choisie
    }

    public void notifier(String message){
        setChanged();
        this.notifyObservers(message);
    }
}
