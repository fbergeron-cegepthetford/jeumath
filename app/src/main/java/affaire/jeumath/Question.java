package affaire.jeumath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Question {

    private final int nombreA, nombreB, reponse;
    private final List<Integer> choix;

    /**
     * Créer une question
     * @param min valeur minimale (peut être négative)
     * @param max valeur maximale (peut être négative)
     */
    public Question(int min, int max){
        Random random = new Random(System.currentTimeMillis()); //initialisé avec un nombre aléatoire

        nombreA = random.nextInt(max - min) + min;
        nombreB = random.nextInt(max - min) + min;
        int decalage1 = random.nextInt(max - min) + min;
        int decalage2 = random.nextInt(max - min) + min;
        int decalage3 = random.nextInt(max - min) + min;

        reponse = nombreA *nombreB;
        int faux1 = nombreA *decalage1;
        int faux2 = decalage2 *nombreB;
        int faux3 = decalage2 *decalage3;

        choix = new ArrayList<>();

        choix.add(reponse);
        choix.add(faux1);
        choix.add(faux2);
        choix.add(faux3);

        Collections.shuffle(choix);
    }

    public int getNombreA() {
        return nombreA;
    }

    public int getNombreB() {
        return nombreB;
    }

    public int getReponse() {
        return reponse;
    }

    public List<Integer> getChoix() {

        return choix;
    }
}
