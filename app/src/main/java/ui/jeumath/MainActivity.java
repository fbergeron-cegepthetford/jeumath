package ui.jeumath;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container_view, new FragmentJeu())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        FragmentManager fm = getSupportFragmentManager();
        switch (item.getItemId()) {
            case R.id.nav_jouer:
                fm.beginTransaction()
                        .replace(R.id.fragment_container_view, new FragmentJeu())
                        .commit();
                return true;
            case R.id.nav_pref:
                fm.beginTransaction()
                        .replace(R.id.fragment_container_view, new FragmentPreferences())
                        .commit();
                return true;
            case R.id.nav_stats:
                fm.beginTransaction()
                        .replace(R.id.fragment_container_view, new FragmentHistorique())
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}