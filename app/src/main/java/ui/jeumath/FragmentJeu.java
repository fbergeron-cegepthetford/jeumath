package ui.jeumath;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import affaire.jeumath.Jeu;
import affaire.jeumath.Question;
import ui.jeumath.databinding.FragmentJeuBinding;


public class FragmentJeu extends Fragment implements Observer {

    Context cx;
    FragmentJeuBinding binding;
    Question question;
    Jeu jeu;

    public FragmentJeu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        binding = FragmentJeuBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        cx = getActivity().getApplicationContext();
        initialiserModele();
        initialiserVue();
    }

    private void initialiserVue() {
        binding.btnReponse1.setOnClickListener(view -> valider(0));
        binding.btnReponse2.setOnClickListener(view -> valider(1));
        binding.btnReponse3.setOnClickListener(view -> valider(2));
        binding.btnReponse4.setOnClickListener(view -> valider(3));

        binding.btnSuivant.setOnClickListener(view -> {
            recupererQuestion();
            binding.btnSuivant.setVisibility(View.INVISIBLE);
            permettreCase(true);
        });

        binding.btnSuivant.setVisibility(View.INVISIBLE);
        recupererQuestion();
    }

    private void valider(int boutton) {
        int maReponse = question.getChoix().get(boutton);
        jeu.valider(maReponse);
    }

    private void initialiserModele() {
        jeu = new Jeu();
        jeu.addObserver(this);
        question = jeu.prochaineQuestion();
    }

    private void permettreCase(boolean permission){
        binding.btnReponse1.setEnabled(permission);
        binding.btnReponse2.setEnabled(permission);
        binding.btnReponse3.setEnabled(permission);
        binding.btnReponse4.setEnabled(permission);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(observable == jeu){
            String message = (String) o;
            if(message.equals("Bon")){
                binding.tvResultat.setText(R.string.bien);
            }
            else if(message.equals("Mauvais")){
                binding.tvResultat.setText(R.string.mal);
            }
            permettreCase(false);
            binding.btnSuivant.setVisibility(View.VISIBLE);
        }
    }

    private void recupererQuestion() {
        question = jeu.prochaineQuestion();
        afficherQuestion();
    }

    private void afficherQuestion() {
        binding.tvQuestion.setText(getString(R.string.question, question.getNombreA(), question.getNombreB()));

        binding.btnReponse1.setText(getString(R.string.entier, question.getChoix().get(0)));
        binding.btnReponse2.setText(getString(R.string.entier, question.getChoix().get(1)));
        binding.btnReponse3.setText(getString(R.string.entier, question.getChoix().get(2)));
        binding.btnReponse4.setText(getString(R.string.entier, question.getChoix().get(3)));
    }
}